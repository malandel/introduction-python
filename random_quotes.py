# coding: utf8

# Import the random module
import random
# Import the jason module
import json

def read_values_json(file, key):
    # Create a new empty list
    values = []
    # Open a json file
    with open(file) as f:
    # Load the data from this file
        data = json.load(f)
    # Add each item in the list
        for entry in data:
            values.append(entry[key])
    # Return the completed list
    return values

# Format message (capitalize each character & quote)
def message(character, quote):
    n_character = character.capitalize()
    n_quote = quote.capitalize()
    return "{} a dit : {}".format(n_character, n_quote)


def get_random_item(my_list):
    # get a random number from 0 to the length of the list (-1 for the index)
    random_number = random.randint(0, len(my_list) - 1)
    # get a random item from a list
    item = my_list[random_number]
    return item # return the item

def random_from_json(file, key):
    all_values = read_values_json(file, key)
    return get_random_item(all_values)

# Program
user_answer = input('Tapez "entrée" pour découvrir une citation ou "B" puis "entrée" pour quitter le programme')

while user_answer != "B":
    # Afficher une quote et un character aléatoires, majuscule à la première lettre
    print(message(random_from_json("characters.json", "character"), random_from_json("quotes.json", "quote")))
    user_answer = input('Tapez "entrée" pour découvrir une autre citation ou "B" puis "entrée" pour quitter le programme')

