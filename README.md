## Learning some Python's basics

#### Some python's exercices made with Open Classrooms

https://openclassrooms.com/fr/courses/4262331-demarrez-votre-projet-avec-python

- **random_quotes**, as indicated by its name, is a small program that allows the user to show random quotes associated with random fictional characters.

- **test_turtle** is a test for the TURTLE module that allows you to generate some drawing


#### Run the program with Linux

- Check which version of Python is installed :

```
python --version

```

* If you have an error, try :

```
python3 --version

```
- Or install Python :

```
sudo apt-get install python3

```

- Clone this project, then run the programs :

```
cd PYTHON
python3 random_quotes.py
python3 test_turtle.py

```
* If you don't have Python3 installed, then type python followed by the name of the file
