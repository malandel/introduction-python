import turtle
turtle.color('red', 'black')
turtle.begin_fill()
while True:
    turtle.forward(200)
    turtle.left(50)
    if abs(turtle.pos()) < 1:
        break
turtle.end_fill()
turtle.done()

# Open your console (Linux)
# Check which version of Python is installed :
# python --version
# if you have an error, try :
# python3 --version
# According to your version : 
# type python3 test_turtle.py or python text-turtle.py to run the program